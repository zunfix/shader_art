use clap::{Parser, Subcommand};
use glium::{
    draw_parameters, glutin, glutin::event_loop::EventLoop, implement_vertex, index::NoIndices,
    uniform, Display, DrawParameters, Program, Surface, VertexBuffer,
};
use std::collections::VecDeque;

#[derive(Parser)]
struct Cli {
    /// Select which art to render
    #[command(subcommand)]
    art: Shader,
}

#[derive(Subcommand)]
enum Shader {
    Gradent1,
    Gradent2,
    Gradent3,
    SlideShow,
}

macro_rules! shader_list {
    ($(($virt: expr, $frag: expr)),*) => {
        {
            let mut s = Vec::new();
            $(
                s.push(
                    (stringify!(src/res/$virt.glsl).replace(" ", ""),
                     stringify!(src/res/$frag.glsl).replace(" ", ""))
                );
            )*
            s
        }
    };
}

fn select_art(display: &Display, cli: Cli) -> VecDeque<Program> {
    let i = match cli.art {
        Shader::Gradent1 => 0,
        Shader::Gradent2 => 1,
        Shader::Gradent3 => 2,
        Shader::SlideShow => 0xFFFF,
    };

    let shaders = shader_list!(
        (position_virt, gradient_1_frag),
        (position_virt, gradient_2_frag),
        (position_virt, gradient_3_frag)
    );

    let mut programs: VecDeque<Program> = VecDeque::new();
    if i == 0xFFFF {
        for s in shaders {
            let vert_shader_src = std::fs::read_to_string(&s.0).unwrap();
            let frag_shader_src = std::fs::read_to_string(&s.1).unwrap();
            programs.push_back(
                glium::Program::from_source(display, &vert_shader_src, &frag_shader_src, None)
                    .unwrap(),
            );
        }
    } else {
        let vert_shader_src = std::fs::read_to_string(&shaders[i].0).unwrap();
        let frag_shader_src = std::fs::read_to_string(&shaders[i].1).unwrap();
        programs.push_back(
            glium::Program::from_source(display, &vert_shader_src, &frag_shader_src, None).unwrap(),
        )
    }

    programs
}

fn main() {
    let cli = Cli::parse();
    let (event_loop, display) = create_display();
    let (vertex_buffer, indices) = generate_vert_buff(&display);

    let mut program = select_art(&display, cli);

    let mut alpha1: f32 = 1.0;
    let mut alpha2: f32 = 0.0;
    let mut frame_cnt = 0;
    event_loop.run(move |event, _, control_flow| {
        *control_flow = wait_for_frame_time();

        match event {
            glutin::event::Event::WindowEvent { event, .. } => match event {
                glutin::event::WindowEvent::CloseRequested => {
                    *control_flow = glutin::event_loop::ControlFlow::Exit;
                    return;
                }
                _ => return,
            },
            glutin::event::Event::NewEvents(cause) => match cause {
                glutin::event::StartCause::ResumeTimeReached { .. } => (),
                glutin::event::StartCause::Init => (),
                _ => return,
            },
            _ => return,
        }

        let mut target = display.draw();

        let draw_params = DrawParameters {
            blend: draw_parameters::Blend::alpha_blending(),
            ..Default::default()
        };

        if program.len() > 1 {
            if frame_cnt < 60 {
                alpha1 = 1.0;
                alpha2 = 0.0;
            } else if frame_cnt < 90 {
                alpha1 -= 1.0 / 30.0;
                alpha2 += 1.0 / 30.0;
            } else if frame_cnt < 150 {
                alpha1 = 1.0;
                alpha2 = 0.0;
                frame_cnt = 0;
                program.rotate_left(1);
            }
        }

        let uniform1 = uniform! {
            alpha: alpha1,
        };

        target.clear_color(0.0, 0.0, 0.0, 1.0);
        target
            .draw(
                &vertex_buffer,
                &indices,
                &program[0],
                &uniform1,
                &draw_params,
            )
            .unwrap();

        if program.len() > 1 {
            let uniform2 = uniform! {
                alpha: alpha2,
            };
            target
                .draw(
                    &vertex_buffer,
                    &indices,
                    &program[1],
                    &uniform2,
                    &draw_params,
                )
                .unwrap();
        }

        frame_cnt += 1;
        target.finish().unwrap();
    });
}

fn create_display() -> (EventLoop<()>, Display) {
    let event_loop = glutin::event_loop::EventLoop::new();
    let wb = glutin::window::WindowBuilder::new();
    let cb = glutin::ContextBuilder::new();
    let display = glium::Display::new(wb, cb, &event_loop).unwrap();
    (event_loop, display)
}

#[derive(Copy, Clone)]
struct Vertex {
    position: [f32; 2],
}

implement_vertex!(Vertex, position);
fn generate_vert_buff(display: &Display) -> (VertexBuffer<Vertex>, NoIndices) {
    let v1 = Vertex {
        position: [-1.0, -1.0],
    };
    let v2 = Vertex {
        position: [-1.0, 1.0],
    };
    let v3 = Vertex {
        position: [1.0, 1.0],
    };
    let v4 = Vertex {
        position: [1.0, -1.0],
    };
    let shape = vec![v1, v2, v3, v3, v4, v1];

    (
        glium::VertexBuffer::new(display, &shape).unwrap(),
        glium::index::NoIndices(glium::index::PrimitiveType::TrianglesList),
    )
}

fn wait_for_frame_time() -> glutin::event_loop::ControlFlow {
    let next_frame_time = std::time::Instant::now() + std::time::Duration::from_nanos(16_666_667);
    glutin::event_loop::ControlFlow::WaitUntil(next_frame_time)
}
