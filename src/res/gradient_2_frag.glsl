#version 330

in vec2 pos;
out vec4 color;

uniform float alpha;

void main() {
    vec2 uv = 0.5 * (pos + 1.0);
    color = vec4(uv, 0.0, alpha);
}
